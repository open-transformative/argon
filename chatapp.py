from src import Host, Client, TorSocket, TorHandler
import time

th = TorHandler.TorHandler()
th.run_tor()

host = Host.HostHandler(listener_port=30405, listener_address="localhost")
host.start()

client_pool = Client.ClientPool(tor_port=30404)


def new_connection_callback(connection: Host.HostConnection):
    connection.set_message_received_callback(new_message_callback)
    connection.put("Welcome son of a Space Mom. Space Mom loves you. <3")


def new_message_callback(connection: Host.HostConnection):
    print(connection.get())


def process_command(command):
    request, *args = command.split(' ')
    if request == 'connect':
        client_pool.make_connection(args[0]).set_message_received_callback(new_message_callback)

#/connect 2lcn52irfkjnfamkpq2yu74742362fwwx5r4cc3ot2o63ldtmd3n4yyd.onion
host.register_new_connection_callback(new_connection_callback)
while True:
    time.sleep(0.1)
    usrInput = input()
    if (usrInput.__len__() > 0) and (usrInput[0] == '/'):
        process_command(usrInput[1:])
    else:
        for connection in host.connections + client_pool.connections:
            connection.put(usrInput)
