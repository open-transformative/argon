from src import TorHandler, Client, Host
import time
import alsaaudio
import threading


def setup_audio():
    global inp, out, provide_audio
    provide_audio = True
    inp = alsaaudio.PCM(alsaaudio.PCM_CAPTURE, alsaaudio.PCM_NONBLOCK, device=device)
    inp.setchannels(1)
    inp.setrate(8000)
    inp.setformat(alsaaudio.PCM_FORMAT_S16_LE)
    out = alsaaudio.PCM(alsaaudio.PCM_PLAYBACK, device=device)
    out.setchannels(1)
    out.setrate(8000)
    out.setformat(alsaaudio.PCM_FORMAT_S16_LE)
    out.setperiodsize(512)
    inp.setperiodsize(512)


def setup_networking():
    global c, device, h,t
    t = TorHandler.TorHandler()
    t.run_tor()
    while not t.__running__ and not t.__error__:
        time.sleep(1)
    print("Running: " + str(t.__running__))
    print("Error: " + str(t.__error__))
    c = Client.ClientPool(tor_port=30404)
    device = 'default'
    h = Host.HostHandler(listener_port=30405, listener_address="localhost")
    h.start()


def play():
    while provide_audio:
        data = None
        for client in list(h.connections):
            data = get_client_data(client, data)
        for client in list(c.connections):
            data = get_client_data(client, data)
        if data is not None:
            out.write(data)
        else:
            time.sleep(1)


def get_client_data(client, data):
    if client.messages_available() > 0:
        d = client.get()
        if d is not None:
            data = d
    while client.messages_available() > 100:
        client.get()
    return data


def record():
    while provide_audio:
        length, data = inp.read()
        if length == 0:
            time.sleep(0.01)
            continue
        for client in list(h.connections):
            client.put(data)
        for client in list(c.connections):
            client.put(data)


def call(host):
    c.make_connection(host + ".onion")


def cd():
    call("2lcn52irfkjnfamkpq2yu74742362fwwx5r4cc3ot2o63ldtmd3n4yyd")


def main():
    global provide_audio,t
    setup_networking()
    setup_audio()
    r = threading.Thread(target=record)
    p = threading.Thread(target=play)
    r.start()
    p.start()
    while True:
        try:
            exec(input("->"))
        except (KeyboardInterrupt,EOFError):
            print("Quiting...")
            provide_audio = False
            h.stop()
            for cl in c.connections:
                cl.serving = False
            t.kill()
            exit(1)


if __name__ == '__main__':
    main()
