import os
import subprocess
import sys
import threading
import signal
import time


class TorHandler:
    def __init__(self, tor_command="tor"):
        self.__tor_command__ = tor_command
        self.__pipe__ = None
        self.__running__ = False
        self.__error__ = False
        self.__started__ = False
        self.__watcher_thread__ = None
        self.__proc__ = None
        self.stdout = []

    def __watch__(self):
        # TODO: Prevents program exit loop break as temporary fix
        # TODO: Implement tor control protocol instead of reading stdout
        for line in self.__proc__.stdout:
            line = line.decode()
            self.stdout.append(line)
            if line.__contains__("[err]"):
                self.__error__ = True
            if line.__contains__("[notice] Bootstrapped 100% (done): Done"):
                self.__running__ = True
            sys.stdout.buffer.flush()
            if self.__running__ or self.__error__:
                break

    def run_tor(self, torrc="./torrc"):
        self.__proc__ = subprocess.Popen([self.__tor_command__, "-f", torrc], shell=False, stdout=subprocess.PIPE)
        self.__watcher_thread__ = threading.Thread(target=self.__watch__)
        self.__watcher_thread__.setDaemon(True)
        self.__watcher_thread__.start()
        self.__started__ = True

    def kill(self):
        # TODO: on exit tor is sometimes running make sure the process is terminated before exit
        self.__proc__.terminate()
        self.__watcher_thread__.join()

    def get_stdout(self):
        return self.stdout
