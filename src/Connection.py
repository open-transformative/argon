import uuid
import _io
import socket
import time
from src import TorSocket, CommunicationQueue
import types
from inspect import signature
from typing import Optional, Union


class Connection:

    def __init__(self, connection_socket: socket.socket, write_file: bool = None, read_file: bool = None):
        self.id = uuid.uuid4()
        self.__socket__ = connection_socket
        if write_file is None:
            write_file = connection_socket.makefile("wb", 0)
        self.__write_file__ = write_file  # type: Optional[_io.BufferedWriter]
        if read_file is None:
            read_file = connection_socket.makefile("rb", -1)
        self.__read_file__ = read_file  # type: Optional[_io.BufferedReader]
        self.socket = None  # type: Union[None,TorSocket.TorSocket,socket.socket]
        self.send_queue = CommunicationQueue.ComQueue()  # type: CommunicationQueue.ComQueue
        self.receive_queue = CommunicationQueue.ComQueue()  # type: CommunicationQueue.ComQueue
        self._message_received_callback = None  # type: Optional[types.FunctionType]
        self._connection_closed_callback = None  # type: Optional[types.FunctionType]
        self.compression = True  # type: bool
        self.serving = False  # type: bool
        self._connected = False  # type: bool
        self._poll_period = 0.01  # type: float

    @property
    def connected(self) -> bool:
        return self._connected

    def set_message_received_callback(self, callback):
        if callable(callback):
            self._message_received_callback = callback

    def set_connection_closed_callback(self, callback):
        if callable(callback):
            self._connection_closed_callback = callback

    def connect(self, hostname, port):
        if isinstance(self.__socket__, TorSocket.TorSocket):
            self.__socket__.connect(hostname, port)

    def get(self) -> object:
        """
Get an object from receive queue
        :return: Deserialized object
        """
        return self.receive_queue.get()

    def put(self, obj: object):
        """
Put an object to send queue
        :param obj: Serializable object to be sent
        """
        self.send_queue.put(obj)

    def get_nowait(self) -> object:
        return self.receive_queue.get_nowait()

    def put_nowait(self, obj: object):
        self.messages_available()
        self.put_nowait(obj)

    def _get_raw_packet_(self) -> bytes:
        return self.send_queue.get_serialized(self.compression)

    def _put_raw_packet(self, payload: bytes):
        self.receive_queue.put_deserialize(payload)

    def messages_available(self) -> int:
        """
Count of messages waiting in receive queue
        :return: message count
        """
        return self.receive_queue.qsize()

    def close(self):
        if self._connected or self.serving:
            self._connected = False
            self.serving = False
            try:
                self.__socket__.shutdown(socket.SHUT_RDWR)
            except OSError:
                pass
            self.__socket__.close()
            if callable(self._connection_closed_callback):
                # if callback has no parameters
                if signature(self._connection_closed_callback).parameters.__len__() == 0:
                    self._connection_closed_callback()
                # if callback has 1 parameter
                elif signature(self._connection_closed_callback).parameters.__len__() == 1:
                    self._connection_closed_callback(self)

    def __serve_client__(self):
        try:
            if not self.send_queue.empty():
                data = self._get_raw_packet_()
                self.__write_file__.write(data.__len__().to_bytes(8, "big"))
                self.__write_file__.write(data)
                self.__write_file__.flush()
            self.__socket__.setblocking(False)
            size = self.__read_file__.read(8)
            if size is None or size.__len__() == 0:
                time.sleep(self._poll_period)
                return
            size_int = int.from_bytes(size, "big")
            if size_int == 0:
                time.sleep(self._poll_period)
                return
            self.__socket__.setblocking(True)
            payload = self.__read_file__.read(size_int)
            self._put_raw_packet(payload)
            if callable(self._message_received_callback):
                # if callback has no parameters
                if signature(self._message_received_callback).parameters.__len__() == 0:
                    self._message_received_callback()
                # if callback has 1 parameter
                elif signature(self._message_received_callback).parameters.__len__() == 1:
                    self._message_received_callback(self)
        except (BrokenPipeError, MemoryError, EOFError, ConnectionResetError, OSError, OverflowError):
            self.close()
