import socket
import socketserver
import threading
from typing import List, Optional
import types
from inspect import signature
from src import Connection

GLOBAL_HOST_HANDLERS = []  # type: List[HostRequestHandler]


class HostConnection(Connection.Connection):
    pass


class HostRequestHandler(socketserver.StreamRequestHandler):
    def __init__(self, request: socket.socket, client_address, server):
        self.connection = None  # type: Optional[HostConnection]
        super().__init__(request, client_address, server)

    def setup(self):
        super().setup()
        self.connection = HostConnection(self.request, self.wfile, self.rfile)
        GLOBAL_HOST_HANDLERS.append(self)
        if callable(self.server.new_connection_callback):
            # if callback has no parameters
            if signature(self.server.new_connection_callback).parameters.__len__() == 0:
                self.server.new_connection_callback()
            # if callback has 1 parameter
            elif signature(self.server.new_connection_callback).parameters.__len__() == 1:
                self.server.new_connection_callback(self.connection)

    def finish(self):
        super().finish()

    def handle(self):
        self.connection.serving = True
        while self.connection.serving:
            self.connection.__serve_client__()
        self.connection.close()
        GLOBAL_HOST_HANDLERS.remove(self)


class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    new_connection_callback = None  # type: Optional[types.FunctionType]


class HostHandler(object):
    handlers = GLOBAL_HOST_HANDLERS

    @property
    def connections(self) -> List[HostConnection]:
        return [x.connection for x in self.handlers]

    def __init__(self, listener_address="localhost", listener_port=80):
        self.__listener_port__ = listener_port
        self.__listener_address__ = listener_address
        self.__TCP_server__ = ThreadedTCPServer((listener_address, listener_port), HostRequestHandler, False)
        self.__TCP_server__.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.__TCP_server__.new_connection_callback = None
        self.__server_process__ = None  # type: Optional[threading.Thread]

    def register_new_connection_callback(self, callback):
        if callable(callback):
            self.__TCP_server__.new_connection_callback = callback

    def start(self):
        self.__server_process__ = threading.Thread(target=self.__TCP_server__.serve_forever)
        self.__server_process__.daemon = True
        self.__TCP_server__.server_bind()
        self.__TCP_server__.server_activate()
        self.__server_process__.start()

    def stop(self):
        for handler in GLOBAL_HOST_HANDLERS:
            handler.connection.serving = False
            handler.connection._connected = False
        self.__TCP_server__.server_close()
        self.__TCP_server__.shutdown()
        self.__server_process__.join()
