import socket


class TorSocket(socket.socket):

    def __init__(self, af=socket.AF_INET, s_type=socket.SOCK_STREAM, proto=-1, socks_host="localhost", socks_port=9050):
        super().__init__(af, s_type, proto)
        self.__socks_port__ = socks_port
        self.__socks_host__ = socks_host
        self.remote_host_info = bytes()
        self.remote_host_type = 0

    def connect(self, host, port=80):
        initial_packet = bytes([0x05, 0x01, 0x00])
        expected_response = bytes([0x05, 0x00])
        connect_request = bytes(
            [0x05, 0x01, 0x00, 0x03] +
            [host.__len__()] +
            list(host.encode(encoding="ascii")) +
            list(port.to_bytes(2, 'big'))
        )
        super().connect((self.__socks_host__, self.__socks_port__))
        self.sendall(initial_packet)
        if self.recv(2) != expected_response:
            self.close()
            raise ConnectionRefusedError("Auth method not allowed")
        self.sendall(connect_request)
        if self.recv(3)[1] != 0:
            self.close()
            raise ConnectionRefusedError("Connection was not established.")
        host_type = self.recv(1)[0]
        self.remote_host_type = host_type
        if host_type == 1:
            self.remote_host_info = self.recv(6)
        if host_type == 4:
            self.remote_host_info = self.recv(18)
        if host_type == 3:
            self.remote_host_info = self.recv(self.recv(1)[0])


