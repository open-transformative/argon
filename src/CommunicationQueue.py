import gzip
import queue
import pickle

class Flags:
    COMPRESSION = 0

    @staticmethod
    def flip_flag(byte_array: bytes, position):
        result = int.from_bytes(byte_array, "big")
        result ^= pow(2, position)
        return result.to_bytes(byte_array.__len__(), "big")

    @staticmethod
    def set_flag(byte_array: bytes, position):
        result = int.from_bytes(byte_array, "big")
        result |= (1 << position)
        return result.to_bytes(byte_array.__len__(), "big")

    @staticmethod
    def clear_flag(byte_array: bytes, position):
        result = int.from_bytes(byte_array, "big")
        result &= ~(1 << position)
        return result.to_bytes(byte_array.__len__(), "big")

    @staticmethod
    def get_flag(byte_array: bytes, position):
        int_type = int.from_bytes(byte_array, "big")
        mask = 1 << position
        return bool(int_type & mask)


def serialize(compress, input_object):
    r_bytes = pickle.dumps(input_object)
    flags = bytes(4)
    if compress:
        r_bytes = gzip.compress(r_bytes)
        flags = Flags.set_flag(flags, Flags.COMPRESSION)
    return flags + r_bytes


def deserialize(data):
    flags = data[0:4]
    data = data[4:]
    if Flags.get_flag(flags, Flags.COMPRESSION):
        data = gzip.decompress(data)
    deserialized_object = pickle.loads(data)
    return deserialized_object


class ComQueue(queue.Queue):
    def __init__(self, max_size=0):
        super().__init__(max_size)

    def get_serialized(self, compress=True):
        return serialize(compress, self.get())

    def put_deserialize(self, data: bytes):
        deserialized_object = deserialize(data)
        self.put(deserialized_object)