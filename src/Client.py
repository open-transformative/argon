import threading
import time
from typing import Dict, List

from src import TorSocket, Connection


class ClientConnection(Connection.Connection):
    pass


def __serve_client__(client):
    while not client.send_queue.empty():
        data = client.send_queue.get_serialized()
        client.write_file.write(data.__len__().to_bytes(8, "big"))
        client.write_file.write(data)
    client.socket.setblocking(False)
    size = client.read_file.read(8)
    if size is None:
        time.sleep(0.1)
        return
    elif size.__len__() == 0:
        time.sleep(0.1)
        return
    int_size = int.from_bytes(size, "big")
    if int_size == 0:
        time.sleep(0.1)
        return
    client.socket.setblocking(True)
    payload = client.read_file.read(int_size)
    client.receive_queue.put_deserialize(payload)


class ClientPool:

    def __init__(self, tor_host="localhost", tor_port="9050"):
        self._connections = []  # type: List[ClientConnection]
        self.tor_host = tor_host
        self.tor_port = tor_port
        self.__threads__ = {}  # type: Dict[ClientConnection,threading.Thread]

    @property
    def connections(self):
        return [x for x in self._connections]

    def make_connection(self, address, port=80):
        connection = ClientConnection(TorSocket.TorSocket(socks_host=self.tor_host, socks_port=self.tor_port))
        self._connections.append(connection)
        self.__threads__[connection] = threading.Thread(target=self.__serve_connection__, args=(address, connection, port))
        self.__threads__[connection].start()
        return connection

    def stop(self):
        for connection in self.connections:
            connection.close()
        for thread in list(self.__threads__.values()):
            thread.join()

    def __serve_connection__(self, address, client: ClientConnection, port=80):
        client.serving = True
        client.connect(address, port)
        while client.serving:
            client.__serve_client__()
        self._connections.remove(client)
        del self.__threads__[client]
