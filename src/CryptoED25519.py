import base64
import hashlib
from secrets import token_bytes
from typing import Optional

import nacl.encoding
import nacl.signing
import nacl.exceptions


class ED25519KeyPair:
    """
    An implementation of ed25519 signing and verification with tor hidden service keys support
    """

    def __init__(self):
        self.__prefix_sk__ = b'== ed25519v1-secret: type0 ==\x00\x00\x00'
        self.__prefix_pk__ = b'== ed25519v1-public: type0 ==\x00\x00\x00'
        self.__tor_domain__ = ".onion"
        self.__tor_public_key__ = None  # type: Optional[bytes]
        self.__tor_secret_key__ = None  # type: Optional[bytes]
        self.__nacl_public_key__ = None  # type: Optional[nacl.signing.VerifyKey]
        self.__nacl_secret_key__ = None  # type: Optional[nacl.signing.SigningKey]
        self.__tor_hostname__ = None  # type: Optional[str]
        self.__seed__ = None  # type: Optional[bytes]

    @property
    def seed(self):
        """
        A seed used for generation of secret keys
        @return: 32 bytes seed or None type if not available
        """
        return self.__seed__

    @property
    def tor_public_key(self) -> Optional[bytes]:
        """
        Tor public key in the same format that is used by hidden service
        @return: tor public key (bytes) or None type if not available
        """
        if self.__tor_public_key__ is None:
            return None
        return self.__prefix_pk__ + self.__tor_public_key__

    @property
    def tor_secret_key(self) -> Optional[bytes]:
        """
        Tor secret key in the same format that is used by hidden service
        @return: tor secret key (bytes) or None type if not available
        """
        if self.__tor_secret_key__ is None:
            return None
        return self.__prefix_sk__ + self.__tor_secret_key__

    @property
    def nacl_public_key(self) -> Optional[nacl.signing.VerifyKey]:
        """
        Nacl verify key that represents a tor hidden service identity (can be used for verification)
        @return: Nacl verify key or None type if not available
        """
        return self.__nacl_public_key__

    @property
    def nacl_secret_key(self) -> Optional[nacl.signing.SigningKey]:
        """
        Nacl secret key that represents a tor hidden service identity (can be used for signing)
        @return: Nacl sign key or None type if not available
        """
        return self.__nacl_secret_key__

    @property
    def tor_hostname(self) -> Optional[str]:
        """
        Tor v3 hostname (for example "urbbvri35wlcwakii3pqfbf3jqypibvfnxvozvwbdl6diidofjoeziyd.onion")
        @return: Hostname (str) or None type if not available
        """
        return self.__tor_hostname__

    def generate_from_seed(self, seed: bytes):
        """
        Generates key-pair from 32 byte seed (resets the key if seed size is not 32 bytes)
        @param seed: 32 byte seed (you can use random bytes)
        @return:
        """
        self.reset()
        if seed.__len__() != 32:
            return
        self.__seed__ = seed
        self.__tor_secret_key_from_seed__(seed)
        self.__nacl_secret_key__ = nacl.signing.SigningKey(seed, nacl.encoding.RawEncoder)
        self.__nacl_public_key__ = self.__nacl_secret_key__.verify_key
        self.__tor_public_key__ = self.__nacl_public_key__.encode(encoder=nacl.encoding.RawEncoder)
        self.__update_tor_hostname__()

    def generate_from_password(self, password: str):
        """
        Generates a key-pair from given password (any unicode str)
        @param password: Unicode string (any length)
        @return:
        """
        password = password.encode(encoding="utf-8")
        seed = hashlib.sha3_256(password).digest()
        self.generate_from_seed(seed)

    def generate_random(self) -> bytes:
        """
        Generates random key-pair
        @return: 32 bytes seed
        """
        self.generate_from_seed(token_bytes(32))
        return self.seed

    def load_nacl_public_key(self, key: nacl.signing.VerifyKey):
        """
        Loads a nacl verify key then sets public keys and tor hostname accordingly
        @param key: Nacl verify key
        @return:
        """
        self.reset()
        self.__nacl_public_key__ = key
        self.__tor_public_key__ = self.__nacl_public_key__.encode(encoder=nacl.encoding.RawEncoder)
        self.__update_tor_hostname__()

    def load_tor_public_key(self, key: bytes):
        """
        Loads a tor public key then sets public keys and tor hostname accordingly
        @param key: Tor public key in same format as used by tor hidden service
        @return:
        """
        self.reset()
        if key.startswith(self.__prefix_pk__):
            key = key[self.__prefix_pk__.__len__():]
        self.__tor_public_key__ = key
        self.__nacl_public_key__ = nacl.signing.VerifyKey(key, nacl.encoding.RawEncoder)
        self.__update_tor_hostname__()

    def load_tor_hostname(self, hostname: str):
        """
        Loads a tor hostname and sets public keys accordingly.
        If the hostname is not valid object is reset
        @param hostname: Tor onion hostname
        (for example "urbbvri35wlcwakii3pqfbf3jqypibvfnxvozvwbdl6diidofjoeziyd.onion")
        @return:
        """
        self.reset()
        if hostname.endswith(self.__tor_domain__):
            hostname = hostname[:-self.__tor_domain__.__len__()]
        address_decoded = base64.b32decode(hostname.upper())
        if address_decoded.__len__() != 35:
            return
        self.__tor_public_key__ = address_decoded[:32]
        self.__nacl_public_key__ = nacl.signing.VerifyKey(address_decoded[:32], nacl.encoding.RawEncoder)
        self.__update_tor_hostname__()
        if self.tor_hostname[:-self.__tor_domain__.__len__()] != hostname:
            self.reset()

    def __update_tor_hostname__(self):
        checksum = bytes(b".onion checksum" + self.__tor_public_key__ + b"\03")
        checksum = hashlib.sha3_256(checksum).digest()[:2]
        tor_hostname = base64.b32encode(self.__tor_public_key__ + checksum + b"\03").decode("ASCII")
        self.__tor_hostname__ = tor_hostname.lower() + self.__tor_domain__

    def __tor_secret_key_from_seed__(self, key):
        sha_digest = hashlib.sha512()
        sha_digest.update(key)
        digest = bytearray(sha_digest.digest())
        digest[0] &= 248
        digest[31] &= 127
        digest[31] |= 64
        self.__tor_secret_key__ = digest
        return digest

    def sign(self, message: bytes) -> Optional[bytes]:
        """
        Signs a given message with a secret key.
        If the secret key is not present returns None type
        @param message: Message as bytes
        @return: Message signature (64 bytes) or None type if secret key is not available
        """
        if self.nacl_secret_key is not None:
            return self.nacl_secret_key.sign(message).signature
        else:
            return None

    def verify(self, message: bytes, signature: bytes) -> Optional[bool]:
        """
        Verifies a signed message and returns True if signature is valid
        If a public (verify) key is not available returns None type
        @param message: A original message
        @param signature: A message signature (64 bytes)
        @return: True if signature is valid, False if invalid, None type if public key is not available
        """
        if signature.__len__() != 64:
            return False
        if self.nacl_public_key is not None:
            try:
                self.nacl_public_key.verify(message, signature)
                return True
            except nacl.exceptions.BadSignatureError:
                return False
        else:
            return None

    def reset(self):
        """
        Resets object to its original state
        @return:
        """
        self.__tor_public_key__ = None
        self.__tor_secret_key__ = None
        self.__nacl_public_key__ = None
        self.__nacl_secret_key__ = None
        self.__tor_hostname__ = None
        self.__seed__ = None
